import type { Preset } from "$lib/local/types/preset.type";
import type { TimerMode } from "$lib/local/types/timerMode.enum";

export type UserLocalPreferences = {
  isEnableAgeCounter: boolean;
  isEnableDailyTarget: boolean;
  dailyFocusTarget?: number;
  breakEndSound?: string;
  focusEndSound?: string;
  sessionFinishSound?: string;
  extendDuration: number;
  presets: Preset[];
  isEnableAutoStartInterval: boolean;
  timerMode: TimerMode;
  intervalTimeLimit?: number;
};
