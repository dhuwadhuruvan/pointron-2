import { ItemType } from "$lib/tidy/types/item.enum";
import {
  SessionBlockType,
  type SessionStore,
  type Task,
} from "$lib/local/types/session.type";
import { generateSessionId } from "$lib/tidy/utils/utils";
import {
  generateBarsFromPreset,
  getTotalDurationFromPreset,
} from "$lib/local/utils/local.utils";
import { get, writable } from "svelte/store";
import {
  Persistance,
  persistLocally,
  retrieveLocally,
} from "$lib/tidy/stores/persistance";
import { SessionPersistance } from "$lib/local/stores/session.persistance";
import type { Tag } from "$lib/local/types/tag.type";
import { TaskPersistance } from "$lib/local/stores/task.persistance";
import { SessionType } from "$lib/local/types/sessionType.enum";
import { SessionState } from "$lib/local/types/sessionState.enum";
import { tick } from "svelte";
import {
  pointronConstants,
  pointronEvents,
  userLocalPreferences,
} from "$lib/local/stores/local.store";
import { PointronEventEnum } from "$lib/local/types/pointronEvent.enum";
import type { Preset } from "../types/preset.type";
import { appStore } from "$lib/tidy/stores/app.store";
import { BarType, type intervalbar } from "../types/intervalbar.type";

const persistance = new Persistance();
const sessionPersistance = new SessionPersistance();
const taskPersistance = new TaskPersistance();
const pointronMiniPlayerPath = "pointron.miniplayer";

export const windowClickEvent = writable(null);

export const todayFocusStore = initTodayFocus();

function initTodayFocus() {
  const retrieve = () => {
    return sessionPersistance.getProgress();
  };
  const { subscribe, set, update } = writable<{
    focus: number;
    streak: number;
  }>(retrieve());
  return {
    subscribe,
    set,
    incrementTodayFocus: (x: number) => {
      update((n: { focus: number; streak: number }) => {
        n.focus += x;
        return n;
      });
    },
    refresh: () => {
      update((n: { focus: number; streak: number }) => {
        const todayFocus = retrieve();
        n.focus = todayFocus.focus;
        n.streak = todayFocus.streak;
        return n;
      });
    },
  };
}

export const sessionChangeEvent = writable<{ id: string | null }>({ id: null });

const seedSessionStore: SessionStore = {
  currentSessionId: undefined,
  previousSessionId: undefined,
  type: SessionType.COUNTUP,
  state: SessionState.NOT_STARTED,
  currentBarIndex: 0,
  timeElapsed: 0,
  currentBarDuration: 0,
  totalElapsed: 0,
  sessionProgress: 0,
  totalExtended: 0,
  plannedDuration: 0,
  bars: [
    {
      duration: 0.0001,
      progress: 1,
      type: BarType.INTERVAL,
    },
  ],
  blocks: [],
  taskBlocks: [],
  scheduledNotifications: [],
};

export const sessionStore = initSessionStore(seedSessionStore);

function initSessionStore(seed: SessionStore) {
  //let timerWorker = new Worker("/timer-worker.ts");
  let timer: string | number | NodeJS.Timer | undefined;
  let idleTimer: any;
  let totalIdleElapsed: number = 0;
  let isIntervalTimeLimitNotified: boolean = false;
  const objectType = ItemType.SessionStoreV2;
  appStore.appendPlayer(pointronMiniPlayerPath);
  const persist = (n: SessionStore) => {
    persistLocally(objectType, n);
  };
  const retrieve = () => {
    let x = retrieveLocally(objectType);
    if (x?.start && typeof x.start == "string") {
      x.start = new Date(x.start);
    }
    if (x?.end && typeof x.end == "string") {
      x.end = new Date(x.end);
    }
    return x;
  };
  //let savedSessionStore = retrieve();
  const { subscribe, set, update } = writable<SessionStore>({ ...seed });
  //if (!savedSessionStore)
  persist(seed);

  const reset = (currentStore: SessionStore) => {
    let newSession: SessionStore = { ...seedSessionStore };
    newSession.previousSessionId = currentStore.currentSessionId;
    newSession.currentSessionId = undefined;
    currentTaskStore.reset();
    clearTimers();
    return newSession;
  };
  const clearTimers = () => {
    clearInterval(timer);
    clearInterval(idleTimer);
  };
  const startSession = (n: SessionStore) => {
    if (Notification.permission !== "granted") {
      let promise = Notification.requestPermission();
    }
    const sessionId = generateSessionId(new Date().getTime());
    n.currentSessionId = sessionId;
    n.state = SessionState.INTERVAL_RUNNING;
    const block = { start: new Date().getTime(), type: SessionBlockType.FOCUS };
    n.blocks.push(block);
    isIntervalTimeLimitNotified = false;
    n.start = new Date();
    n = resumeTimer(n);
    sessionChangeEvent.set({ id: sessionId });
    return n;
  };
  const refreshCurrentBarDuration = (n: SessionStore) => {
    return n.bars[n.currentBarIndex]?.duration;
  };
  const resumeTimer = (n: SessionStore, isResetTimer: boolean = true) => {
    if (isResetTimer) n.timeElapsed = 0;
    clearTimers();
    let currentBlockStartTime = n.blocks[n.blocks.length - 1].start;
    timer = setInterval(() => {
      n.scheduledNotifications = [];
      const currentTime = new Date().getTime();
      n.totalElapsed = (currentTime - n.blocks[0].start) / 1000;
      n.timeElapsed = (currentTime - currentBlockStartTime) / 1000;
      let timeLimit = get(userLocalPreferences).intervalTimeLimit;
      let sessionTimeRemaining: number | undefined = undefined;
      let timeRemainingToTimeLimit: number | undefined = undefined;
      if (
        n.type != SessionType.PRESET &&
        n.state === SessionState.INTERVAL_RUNNING &&
        timeLimit
      ) {
        timeRemainingToTimeLimit = timeLimit * 60 - n.timeElapsed;
        if (timeRemainingToTimeLimit < 1 && !isIntervalTimeLimitNotified) {
          pointronEvents.notify(PointronEventEnum.INTERVAL_TIME_LIMIT_REACHED);
          isIntervalTimeLimitNotified = true;
        }
      }
      if (n.type != SessionType.COUNTUP) {
        sessionTimeRemaining = n.plannedDuration - n.totalElapsed;
        if (sessionTimeRemaining < 0) clearTimers();
        if (n.type == SessionType.DYNAMIC && sessionTimeRemaining < 0) {
          n.state = SessionState.TIME_IS_UP;
          pointronEvents.notify(PointronEventEnum.SESSION_TIME_IS_UP);
        } else if (
          n.type == SessionType.DYNAMIC &&
          sessionTimeRemaining < get(pointronConstants).runningOutDuration &&
          (n.state == SessionState.INTERVAL_RUNNING ||
            n.state == SessionState.TIME_IS_UP)
        ) {
          n.state = SessionState.TIME_IS_RUNNING_OUT;
        }
        n.scheduledNotifications.push({
          inSeconds: sessionTimeRemaining,
          message: "Session time is up. Please extend or finish the session",
        });
      }
      if (
        (n.type === SessionType.COUNTUP && timeRemainingToTimeLimit) ||
        (n.type != SessionType.COUNTUP &&
          sessionTimeRemaining &&
          timeRemainingToTimeLimit &&
          sessionTimeRemaining > timeRemainingToTimeLimit)
      )
        n.scheduledNotifications.push({
          inSeconds: timeRemainingToTimeLimit,
          message:
            "Its been " +
            timeLimit +
            " minutes on this interval. Please take a break or continue.",
        });
      n.sessionProgress = refreshSessionProgress(n) ?? 0;
      persist(n);
      n.bars = refreshProgressOnBars(n);
      sessionStore.set(n);
    }, 1000);
    persist(n);
    return n;
  };
  const reinitializeBars = (n: SessionStore) => {
    let bars: intervalbar[] = [];
    if (n.type === SessionType.PRESET && n.selectedPreset) {
      bars = generateBarsFromPreset(n.selectedPreset);
    } else if (n.type === SessionType.DYNAMIC && n.plannedDuration) {
      bars = [
        ...bars,
        {
          duration: n.plannedDuration,
          progress: 0,
          type: BarType.INTERVAL,
        },
      ];
    } else if (n.type === SessionType.COUNTUP) {
      bars = [
        ...bars,
        {
          duration: 0.0001,
          progress: 1,
          type: BarType.INTERVAL,
        },
      ];
    }
    return bars;
  };
  function continueSession(n: SessionStore) {
    if (n.state == SessionState.INTERVAL_RUNNING) {
      pointronEvents.notify(PointronEventEnum.INTERVAL_ENDED);
      n.state = SessionState.BREAK_RUNNING;
      const block = {
        start: new Date().getTime(),
        type: SessionBlockType.BREAK,
      };
      n.blocks.push(block);
    } else {
      pointronEvents.notify(PointronEventEnum.BREAK_ENDED);
      n.state = SessionState.INTERVAL_RUNNING;
      const block = {
        start: new Date().getTime(),
        type: SessionBlockType.FOCUS,
      };
      n.blocks.push(block);
      isIntervalTimeLimitNotified = false;
    }
    n = resumeTimer(n);
    return n;
  }
  function calculateEndTime(n: SessionStore) {
    let endTime = new Date();
    if (n.state === SessionState.NOT_STARTED) {
      endTime = new Date(new Date().getTime() + n.plannedDuration * 1000);
    } else if (n.start) {
      endTime = new Date(n.start.getTime() + n.plannedDuration * 1000);
    }
    return endTime;
  }
  function refreshSessionProgress(n: SessionStore) {
    if (n.type == SessionType.COUNTUP) return 100;
    if (!n.start || !n.end) return;
    return (
      (n.totalElapsed / ((n.end?.getTime() - n.start?.getTime()) / 1000)) * 100
    );
  }
  function startIdleTime() {
    idleTimer = setInterval(() => {
      totalIdleElapsed += 1;
    }, 1000);
  }
  function refreshProgressOnBars(n: SessionStore) {
    if (n.type == SessionType.COUNTUP) {
      if (n.bars && n.bars.length < 1) return n.bars;
      const currentLastBar = n.bars.pop();
      if (!currentLastBar) return n.bars;
      let lastBar = {
        ...currentLastBar,
        duration: currentLastBar.duration + 1,
      };
      return [...n.bars, lastBar];
    }
    let totalElapsedRemaining = n.totalElapsed;
    let newBars: Array<intervalbar> = [];
    n.bars.forEach((bar) => {
      let barDuration = bar.duration;
      let refreshedProgress = 0;
      if (barDuration == totalElapsedRemaining) {
        refreshedProgress = 1;
        totalElapsedRemaining = 0;
        //todo - fix this - creating infinite loop
        clearTimers();
        if (n.currentBarIndex == n.bars.length - 1) {
          sessionStore.finishSession();
        } else if (get(userLocalPreferences).isEnableAutoStartInterval) {
          n.currentBarIndex += 1;
          sessionStore.continueSession();
        } else {
          n.state =
            n.state === SessionState.INTERVAL_RUNNING
              ? SessionState.INTERVAL_COMPLETED
              : SessionState.BREAK_COMPLETED;
          n.currentBarIndex += 1;
          if (n.state === SessionState.INTERVAL_COMPLETED) {
            pointronEvents.notify(PointronEventEnum.INTERVAL_ENDED);
          } else if (n.state === SessionState.BREAK_COMPLETED) {
            pointronEvents.notify(PointronEventEnum.BREAK_ENDED);
          }
          startIdleTime();
        }
      } else if (barDuration < totalElapsedRemaining) {
        refreshedProgress = 1;
        totalElapsedRemaining = totalElapsedRemaining - barDuration;
      } else {
        refreshedProgress =
          1 - (barDuration - totalElapsedRemaining) / barDuration;
        totalElapsedRemaining = 0;
      }
      newBars = [...newBars, { ...bar, progress: refreshedProgress }];
    });
    return newBars;
  }
  function resumeSession(n: SessionStore) {
    if (n.state === SessionState.INTERVAL_RUNNING) return n;
    if (n.state === SessionState.BREAK_COMPLETED) {
      clearInterval(idleTimer);
    }
    if (n.type == SessionType.COUNTUP) {
      n.bars = [
        ...n.bars,
        {
          duration: 0,
          progress: 1,
          type: BarType.INTERVAL,
        },
      ];
      n.currentBarIndex += 1;
    } else if (n.state === SessionState.BREAK_RUNNING) {
      let oldBars = n.bars;
      n.bars = [];
      n.bars = oldBars.slice(0, n.currentBarIndex);
      n.bars = [
        ...n.bars,
        {
          duration: n.timeElapsed,
          progress: 1,
          type: BarType.BREK,
        },
      ];
      if (n.type === SessionType.DYNAMIC) {
        n.bars = [
          ...n.bars,
          {
            duration: n.currentBarDuration - n.timeElapsed,
            progress: 0,
            type: BarType.INTERVAL,
          },
        ];
      }
      n.bars = n.bars.concat(oldBars.slice(n.currentBarIndex + 1));
      n.currentBarIndex += 1;
    } else if (n.state === SessionState.INTERVAL_COMPLETED) {
      let oldBars = n.bars;
      n.bars = [];
      n.bars = oldBars.slice(0, n.currentBarIndex);
      n.bars = n.bars.concat(oldBars.slice(n.currentBarIndex + 1));
    }
    n.state = SessionState.INTERVAL_RUNNING;
    const block = {
      start: new Date().getTime(),
      end: null,
      type: SessionBlockType.FOCUS,
    };
    n.blocks = [...n.blocks, block];
    isIntervalTimeLimitNotified = false;
    resumeTimer(n);
    n.currentBarDuration = refreshCurrentBarDuration(n);
    return n;
  }
  return {
    subscribe,
    set: (m: SessionStore) => {
      set(m);
    },
    reset: () => {
      set(seed);
    },
    reload: () => {
      let saved = retrieve();
      set(saved);
    },
    finishSession: () => {
      update((n: SessionStore) => {
        sessionChangeEvent.set({ id: null });
        const m = {
          elapsed: n.totalElapsed,
          extended: n.totalExtended,
          start: n.start?.getTime(),
          end: n.end?.getTime(),
          id: n.currentSessionId ?? generateSessionId(new Date().getTime()),
          intention: focus,
          label: "",
          blocks: n.blocks,
        };
        persistance.create(m, ItemType.Sessions);
        const tasks = get(currentTaskStore)?.map((t: Task) => {
          t.sessionId = m.id;
          return t;
        });
        if (tasks && tasks.length > 0) {
          taskPersistance.createTasks(tasks);
        }
        n = reset(n);
        persist(n);
        return n;
      });
      todayFocusStore.refresh();
      pointronEvents.notify(PointronEventEnum.REFRESH_TIMELINE);
    },
    resetSession: () => {
      update((n: SessionStore) => {
        sessionChangeEvent.set({ id: null });
        n = reset(n);
        persist(n);
        return n;
      });
      pointronEvents.notify(PointronEventEnum.REFRESH_TIMELINE);
    },
    deleteSession: () => {
      update((n: SessionStore) => {
        if (n.currentSessionId || n.previousSessionId) {
          persistance.delete(
            n.currentSessionId ?? n.previousSessionId ?? "",
            ItemType.Sessions
          );
        }
        n = reset(n);
        persist(n);
        return n;
      });
      todayFocusStore.refresh();
    },
    startTask: (id: string, previousWorked: number) => {
      update((n: SessionStore) => {
        const taskBlock = {
          taskId: id,
          start: new Date().getTime(),
          previousWorked,
        };
        n.taskBlocks = [...n.taskBlocks, taskBlock];
        if (n.state === SessionState.NOT_STARTED) {
          n = startSession(n);
        } else if (n.state === SessionState.BREAK_RUNNING) {
          n = resumeSession(n);
        }
        //todo
        //appEvents.notify(EventType.THINMODE_PANELSWITCH, 1)
        persist(n);
        return n;
      });
    },
    stopCurrentTask: () => {
      update((n: SessionStore) => {
        const taskBlock = { taskId: null, start: new Date().getTime() };
        n.taskBlocks = [...n.taskBlocks, taskBlock];
        persist(n);
        return n;
      });
    },
    resumeTimer: (isResetTimer: boolean = true) => {
      update((n: SessionStore) => {
        n = resumeTimer(n, isResetTimer);
        return n;
      });
    },
    startBreak: () => {
      update((n: SessionStore) => {
        if (n.state === SessionState.BREAK_RUNNING) return n;
        if (n.state === SessionState.INTERVAL_COMPLETED) {
          clearInterval(idleTimer);
        }
        if (n.type == SessionType.COUNTUP) {
          n.bars = [
            ...n.bars,
            {
              duration: 0,
              progress: 1,
              type: BarType.BREK,
            },
          ];
          n.currentBarIndex += 1;
        } else if (
          n.type === SessionType.DYNAMIC ||
          n.state === SessionState.INTERVAL_RUNNING
        ) {
          let oldBars = n.bars;
          n.bars = [];
          n.bars = oldBars.slice(0, n.currentBarIndex);
          n.bars = [
            ...n.bars,
            {
              duration: n.timeElapsed,
              progress: 1,
              type: BarType.INTERVAL,
            },
          ];
          n.bars = [
            ...n.bars,
            {
              duration: n.currentBarDuration - n.timeElapsed,
              progress: 0,
              type: BarType.BREK,
            },
          ];
          let remainingBars = oldBars.slice(n.currentBarIndex + 1);
          n.bars = n.bars.concat(remainingBars);
          n.currentBarIndex += 1;
        }
        n.state = SessionState.BREAK_RUNNING;
        const block = {
          start: new Date().getTime(),
          type: SessionBlockType.BREAK,
        };
        n.blocks = [...n.blocks, block];
        n = resumeTimer(n);
        n.currentBarDuration = refreshCurrentBarDuration(n);
        return n;
      });
    },
    resumeSession: () => {
      update((n: SessionStore) => {
        n = resumeSession(n);
        return n;
      });
    },
    clearIntervals: () => {
      update((n: SessionStore) => {
        clearTimers();
        return n;
      });
    },
    clearScheduledNotifications: () => {
      update((n: SessionStore) => {
        n.scheduledNotifications = [];
        return n;
      });
    },
    extendSession: () => {
      let extendDurationSetting = get(userLocalPreferences).extendDuration * 60;
      update((n: SessionStore) => {
        n.totalExtended = n.totalExtended + extendDurationSetting;
        n.plannedDuration = n.plannedDuration + extendDurationSetting;
        n.currentBarDuration = n.currentBarDuration + extendDurationSetting;
        if (!n.plannedDuration) return n;
        n.end = calculateEndTime(n);
        let currentLastBar = n.bars.pop();
        if (currentLastBar) {
          let lastBar = {
            ...currentLastBar,
            duration: n.currentBarDuration,
          };
          n.bars = [...n.bars, lastBar];
        }
        if (n.state === SessionState.TIME_IS_UP) {
          resumeTimer(n, false);
        }
        return n;
      });
    },
    startSession: () => {
      update((n: SessionStore) => {
        n.bars = reinitializeBars(n);
        n.currentBarDuration = refreshCurrentBarDuration(n);
        n = startSession(n);
        persist(n);
        return n;
      });
    },
    continueSession: () => {
      update((n: SessionStore) => {
        n = continueSession(n);
        return n;
      });
    },
    onPresetSelection: (preset: Preset) => {
      update((n: SessionStore) => {
        n.selectedPreset = preset;
        n.type = SessionType.PRESET;
        n.plannedDuration = getTotalDurationFromPreset(preset);
        n.end = calculateEndTime(n);
        n.bars = reinitializeBars(n);
        n.currentBarDuration = refreshCurrentBarDuration(n);
        persist(n);
        return n;
      });
    },
    onDynamicDurationChange: (duration: number) => {
      update((n: SessionStore) => {
        if (duration === 0) {
          n.type = SessionType.COUNTUP;
          n.plannedDuration = 0;
        } else {
          if (!get(appStore).isDebugMode) {
            duration = duration * 60;
          }
          n.plannedDuration = duration;
          n.type = SessionType.DYNAMIC;
        }
        n.end = calculateEndTime(n);
        n.bars = reinitializeBars(n);
        n.currentBarDuration = refreshCurrentBarDuration(n);
        persist(n);
        return n;
      });
    },
  };
}

export const currentTaskStore = initCurrentTaskStore();

function initCurrentTaskStore() {
  const objectType = ItemType.CurrentTask;
  const persist = (n: Task[]) => {
    persistLocally(objectType, n);
  };
  const retrieve = () => {
    return retrieveLocally(objectType);
  };
  const seed = retrieve();
  const { subscribe, set, update } = writable<Task[]>(seed ?? []);
  return {
    subscribe,
    set: (m: Task[]) => {
      set(m);
    },
    reset: () => {
      set([]);
      persist([]);
    },
    updateTask: (task: Task) => {
      update((n: Task[]) => {
        if (n && n.length > 0) {
          n = n.filter((t) => t.id != task.id);
          n.push(task);
        }
        persist(n);
        return n;
      });
    },
    updateCurrentTasks: (tasks: Task[]) => {
      update((n: Task[]) => {
        n = tasks;
        persist(n);
        return n;
      });
    },
    deleteTask: (id: string) => {
      update((n: Task[]) => {
        if (n && n.length > 0) {
          n = n.filter((t) => t.id != id);
        }
        persist(n);
        return n;
      });
    },
  };
}

export const tagStore = initTagStore();

function initTagStore() {
  const objectType = ItemType.Tag;
  const savedTags = persistance.retrieve(objectType);
  const { subscribe, set, update } = writable<Tag[]>(savedTags ?? []);
  const refresh = () => {
    return persistance.retrieve(objectType);
  };
  return {
    subscribe,
    set,
    create: (tag: Tag) => {
      persistance.create(tag, objectType);
      update((x: Tag[]) => {
        x.push(tag);
        return x;
      });
    },
    update: (tag: Tag) => {
      persistance.update(tag, objectType);
      update((x: Tag[]) => {
        x = x.filter((t) => t.id != tag.id);
        x.push(tag);
        return x;
      });
    },
    delete: (id: string) => {
      persistance.delete(id, objectType);
      update((x: Tag[]) => {
        x = x.filter((t) => t.id != id);
        return x;
      });
    },
    retrieve: () => {
      subscribe((x: Tag[]) => {
        return x;
      });
    },
    search: (query: string) => {},
  };
}
