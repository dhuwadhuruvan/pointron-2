import { writable } from "svelte/store";
import { PointronEventEnum } from "$lib/local/types/pointronEvent.enum";
import type { PointronEvent } from "$lib/local/types/pointronEvent.type";
import type { PointronConstants } from "$lib/local/types/pointronConstants.type";
import type { UserLocalPreferences } from "$lib/local/types/userLocalPreferences.type";
import { persistLocally, retrieveLocally } from "$lib/tidy/stores/persistance";
import { ItemType } from "$lib/tidy/types/item.enum";
import { TimerMode } from "$lib/local/types/timerMode.enum";
import type { Preset } from "../types/preset.type";
import { generateUID } from "$lib/tidy/utils/utils";
import type { LocalStore } from "$lib/tidy/types/localStore.type";
import subatoms from "$lib/tidy/data/subatoms.json";

const appName = "Pointron";

let subatom: any = subatoms.find((subatom: any) => subatom.name == appName);

export const localStore = initLocalStore({ appName, subatom });

function initLocalStore(seed: LocalStore) {
  const { subscribe, set, update } = writable<LocalStore>(seed);
  return {
    subscribe,
    set,
    update,
  };
}

const seedPresets = [
  // { id: generateUID(), rounds: 1, duration: 5, brek: 2 },
  { id: generateUID(), rounds: 3, duration: 10, brek: 2 },
  { id: generateUID(), rounds: 4, duration: 25, brek: 5 },
  {
    id: generateUID(),
    name: "hackathon",
    rounds: 4,
    duration: 10,
    brek: 2,
    additional: [
      { id: generateUID(), rounds: 1, duration: 20, brek: 0 },
      { id: generateUID(), rounds: 1, duration: 20, brek: 0 },
      { id: generateUID(), rounds: 1, duration: 20, brek: 0 },
      { id: generateUID(), rounds: 1, duration: 20, brek: 0 },
    ],
  },
  // { id: generateUID(), rounds: 3, duration: 50 * 60, brek: 2 * 60 },
  // { id: generateUID(), rounds: 4, duration: 10 * 60, brek: 2 * 60 },
  // { id: generateUID(), rounds: 3, duration: 50 * 60, brek: 2 * 60 },
  // { id: generateUID(), rounds: 4, duration: 10 * 60, brek: 2 * 60 },
  // { id: generateUID(), rounds: 3, duration: 50 * 60, brek: 2 * 60 },
  // { id: generateUID(), rounds: 4, duration: 10 * 60, brek: 2 * 60 },
  // { id: generateUID(), rounds: 3, duration: 50 * 60, brek: 2 * 60 },
];

export const userLocalPreferences = initUserLocalPreferences({
  isEnableAgeCounter: true,
  isEnableDailyTarget: true,
  dailyFocusTarget: 10,
  extendDuration: 5,
  presets: seedPresets,
  isEnableAutoStartInterval: false,
  timerMode: TimerMode.JOURNAL,
});

function initUserLocalPreferences(seed: UserLocalPreferences) {
  const objectType = ItemType.UserLocalPreferences;
  let savedPreferences = retrieveLocally(objectType);
  const { subscribe, set, update } = writable<UserLocalPreferences>(
    savedPreferences ?? seed
  );
  if (!savedPreferences) persistLocally(objectType, seed);
  return {
    subscribe,
    set: (m: UserLocalPreferences) => {
      persistLocally(objectType, m);
      set(m);
    },
    reload: () => {
      let savedPreferences = retrieveLocally(objectType);
      set(savedPreferences);
    },
    updatePreset: (preset: Preset) => {
      update((m: UserLocalPreferences) => {
        let n = m.presets;
        let currentPresetIndex = n.findIndex((p) => p.id == preset.id);
        let presetsToRight = n.slice(currentPresetIndex + 1);
        n = n.slice(0, currentPresetIndex);
        n = [...n, preset];
        n = n.concat(presetsToRight);
        m.presets = n;
        persistLocally(objectType, m);
        return m;
      });
    },
    removePreset: (presetId: string) => {
      update((m: UserLocalPreferences) => {
        let n = m.presets;
        n = n.filter((x: Preset) => x.id != presetId);
        m.presets = n;
        persistLocally(objectType, m);
        return m;
      });
    },
    addPreset: (preset: Preset) => {
      update((n) => {
        n.presets.push(preset);
        persistLocally(objectType, n);
        return n;
      });
    },
  };
}

export const pointronEvents = initEventStore({
  event: PointronEventEnum.NONE,
  value: false,
});

function initEventStore(seed: PointronEvent) {
  const { subscribe, set, update } = writable<PointronEvent>(seed);
  return {
    subscribe,
    set: (m: PointronEvent) => {
      set(m);
    },
    reset: () => {
      update((n: PointronEvent) => {
        return { ...n, event: PointronEventEnum.NONE };
      });
    },
    notify: (m: PointronEventEnum, value: any = undefined) => {
      update((n: PointronEvent) => {
        return { ...n, value, event: m };
      });
    },
  };
}

export const appMenu: string[] = ["point", "flow", "settings"];

export const pointronConstants = initPointronConstants({
  timerModes: ["Minimal", "Journal"],
  focusPlaceholderText: [
    "cooking ice cream",
    "cleaning wordle",
    "coding dishes",
    "showering",
    "draining umbrella",
    "commanding alexa",
  ],
  runningOutDuration: 5,
  gapThreshold: 60,
});

function initPointronConstants(seed: PointronConstants) {
  const { subscribe, set, update } = writable<PointronConstants>(seed);
  return {
    subscribe,
    set: (m: PointronConstants) => {
      set(m);
    },
    update,
  };
}

export const oasisOidcConfig = {
  authority: "https://auth.oasislabs.com",
  // Replace with your app's frontend client ID.
  client_id: import.meta.env.VITE_OASIS_CLIENT_ID,
  redirect_uri: `${window.location.origin}/play`,
  response_type: "code",
  scope: "openid profile email parcel.public",
  filterProtocolClaims: false,
  loadUserInfo: false,
  extraQueryParams: {
    audience: "https://api.oasislabs.com/parcel",
  },
  extraTokenParams: {
    audience: "https://api.oasislabs.com/parcel",
  },
};

export const seedSessions = [
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 13),
    endTime: new Date(2022, 1, 11, 14),
    id: new Date(2022, 1, 11, 13).getTime(),
    intention: "",
  },
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 14, 30),
    id: new Date(2022, 1, 11, 14, 30).getTime(),
    endTime: new Date(2022, 1, 11, 17),
    intention: "ID Card insurance project",
  },
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 17),
    id: new Date(2022, 1, 11, 17).getTime(),
    endTime: new Date(2022, 1, 11, 17, 32),
    intention: "de picto",
  },
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 19, 21),
    id: new Date(2022, 1, 11, 19, 21).getTime(),
    endTime: new Date(2022, 1, 11, 23),
    intention: "",
  },
];

export const seedTasks = [
  { label: "first task", estimate: 0, workedFor: 15, checked: true },
  {
    label: "second sdfasdfs task",
    estimate: 35,
    workedFor: 15,
    checked: false,
    isInprogress: true,
  },
  { label: "third task", estimate: 35, workedFor: 45, checked: false },
];
