let timeElapsed = 0;
let timerId = null;
let timers = [];

function updateTimer() {
  timeElapsed++;
  timerId = setTimeout(updateTimer, 1000);
}

self.onmessage = (event) => {
  if (event.data === "start" && timerId === null) {
    updateTimer();
  } else if (event.data === "stop" && timerId !== null) {
    clearTimeout(timerId);
    timerId = null;
  } else if (event.data === "reset") {
    postMessage(timeElapsed);
    timeElapsed = 0;
  } else if (event.data === "get") {
    postMessage(timeElapsed);
  }
};

async function showNotification(message) {
  const options = {
    body: message,
    icon: "/pointron.png",
  };
  await self.registration.showNotification("Time is up ", options);
}

self.addEventListener("activate", (event) => {});

self.addEventListener("notificationclick", (event) => {
  event.notification.close();
  clients.openWindow("/");
});

self.addEventListener("message", async (event) => {
  if (event.data && event.data.type === "SCHEDULE_NOTIFICATIONS") {
    const { notifications } = event.data;
    notifications.forEach((notification) => {
      const { message, inSeconds } = notification;
      timers.push(
        setTimeout(() => {
          showNotification(message);
        }, inSeconds * 1000)
      );
    });
  } else if (event.data && event.data.type === "CLEAR_NOTIFICATIONS") {
    timers.forEach((timer) => clearTimeout(timer));
  }
});
